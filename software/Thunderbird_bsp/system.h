/*
 * system.h - SOPC Builder system and BSP software package information
 *
 * Machine generated for CPU 'cpu' in SOPC Builder design 'new_core'
 * SOPC Builder design path: C:/AlteraPrj/ThunderBird/new_core.sopcinfo
 *
 * Generated: Sun Mar 07 20:32:36 CST 2021
 */

/*
 * DO NOT MODIFY THIS FILE
 *
 * Changing this file will have subtle consequences
 * which will almost certainly lead to a nonfunctioning
 * system. If you do modify this file, be aware that your
 * changes will be overwritten and lost when this file
 * is generated again.
 *
 * DO NOT MODIFY THIS FILE
 */

/*
 * License Agreement
 *
 * Copyright (c) 2008
 * Altera Corporation, San Jose, California, USA.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * This agreement shall be governed in all respects by the laws of the State
 * of California and by the laws of the United States of America.
 */

#ifndef __SYSTEM_H_
#define __SYSTEM_H_

/* Include definitions from linker script generator */
#include "linker.h"


/*
 * CPU configuration
 *
 */

#define ALT_CPU_ARCHITECTURE "altera_nios2_gen2"
#define ALT_CPU_BIG_ENDIAN 0
#define ALT_CPU_BREAK_ADDR 0x00040820
#define ALT_CPU_CPU_ARCH_NIOS2_R1
#define ALT_CPU_CPU_FREQ 50000000u
#define ALT_CPU_CPU_ID_SIZE 1
#define ALT_CPU_CPU_ID_VALUE 0x00000000
#define ALT_CPU_CPU_IMPLEMENTATION "tiny"
#define ALT_CPU_DATA_ADDR_WIDTH 0x13
#define ALT_CPU_DCACHE_LINE_SIZE 0
#define ALT_CPU_DCACHE_LINE_SIZE_LOG2 0
#define ALT_CPU_DCACHE_SIZE 0
#define ALT_CPU_EXCEPTION_ADDR 0x00020020
#define ALT_CPU_FLASH_ACCELERATOR_LINES 0
#define ALT_CPU_FLASH_ACCELERATOR_LINE_SIZE 0
#define ALT_CPU_FLUSHDA_SUPPORTED
#define ALT_CPU_FREQ 50000000
#define ALT_CPU_HARDWARE_DIVIDE_PRESENT 0
#define ALT_CPU_HARDWARE_MULTIPLY_PRESENT 0
#define ALT_CPU_HARDWARE_MULX_PRESENT 0
#define ALT_CPU_HAS_DEBUG_CORE 1
#define ALT_CPU_HAS_DEBUG_STUB
#define ALT_CPU_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define ALT_CPU_HAS_JMPI_INSTRUCTION
#define ALT_CPU_ICACHE_LINE_SIZE 0
#define ALT_CPU_ICACHE_LINE_SIZE_LOG2 0
#define ALT_CPU_ICACHE_SIZE 0
#define ALT_CPU_INST_ADDR_WIDTH 0x13
#define ALT_CPU_NAME "cpu"
#define ALT_CPU_OCI_VERSION 1
#define ALT_CPU_RESET_ADDR 0x00020000


/*
 * CPU configuration (with legacy prefix - don't use these anymore)
 *
 */

#define NIOS2_BIG_ENDIAN 0
#define NIOS2_BREAK_ADDR 0x00040820
#define NIOS2_CPU_ARCH_NIOS2_R1
#define NIOS2_CPU_FREQ 50000000u
#define NIOS2_CPU_ID_SIZE 1
#define NIOS2_CPU_ID_VALUE 0x00000000
#define NIOS2_CPU_IMPLEMENTATION "tiny"
#define NIOS2_DATA_ADDR_WIDTH 0x13
#define NIOS2_DCACHE_LINE_SIZE 0
#define NIOS2_DCACHE_LINE_SIZE_LOG2 0
#define NIOS2_DCACHE_SIZE 0
#define NIOS2_EXCEPTION_ADDR 0x00020020
#define NIOS2_FLASH_ACCELERATOR_LINES 0
#define NIOS2_FLASH_ACCELERATOR_LINE_SIZE 0
#define NIOS2_FLUSHDA_SUPPORTED
#define NIOS2_HARDWARE_DIVIDE_PRESENT 0
#define NIOS2_HARDWARE_MULTIPLY_PRESENT 0
#define NIOS2_HARDWARE_MULX_PRESENT 0
#define NIOS2_HAS_DEBUG_CORE 1
#define NIOS2_HAS_DEBUG_STUB
#define NIOS2_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define NIOS2_HAS_JMPI_INSTRUCTION
#define NIOS2_ICACHE_LINE_SIZE 0
#define NIOS2_ICACHE_LINE_SIZE_LOG2 0
#define NIOS2_ICACHE_SIZE 0
#define NIOS2_INST_ADDR_WIDTH 0x13
#define NIOS2_OCI_VERSION 1
#define NIOS2_RESET_ADDR 0x00020000


/*
 * Define for each module class mastered by the CPU
 *
 */

#define __ALTERA_AVALON_JTAG_UART
#define __ALTERA_AVALON_ONCHIP_MEMORY2
#define __ALTERA_AVALON_PIO
#define __ALTERA_AVALON_SYSID_QSYS
#define __ALTERA_AVALON_TIMER
#define __ALTERA_NIOS2_GEN2


/*
 * System configuration
 *
 */

#define ALT_DEVICE_FAMILY "MAX 10"
#define ALT_ENHANCED_INTERRUPT_API_PRESENT
#define ALT_IRQ_BASE NULL
#define ALT_LOG_PORT "/dev/null"
#define ALT_LOG_PORT_BASE 0x0
#define ALT_LOG_PORT_DEV null
#define ALT_LOG_PORT_TYPE ""
#define ALT_NUM_EXTERNAL_INTERRUPT_CONTROLLERS 0
#define ALT_NUM_INTERNAL_INTERRUPT_CONTROLLERS 1
#define ALT_NUM_INTERRUPT_CONTROLLERS 1
#define ALT_STDERR "/dev/jtag_uart"
#define ALT_STDERR_BASE 0x41078
#define ALT_STDERR_DEV jtag_uart
#define ALT_STDERR_IS_JTAG_UART
#define ALT_STDERR_PRESENT
#define ALT_STDERR_TYPE "altera_avalon_jtag_uart"
#define ALT_STDIN "/dev/jtag_uart"
#define ALT_STDIN_BASE 0x41078
#define ALT_STDIN_DEV jtag_uart
#define ALT_STDIN_IS_JTAG_UART
#define ALT_STDIN_PRESENT
#define ALT_STDIN_TYPE "altera_avalon_jtag_uart"
#define ALT_STDOUT "/dev/jtag_uart"
#define ALT_STDOUT_BASE 0x41078
#define ALT_STDOUT_DEV jtag_uart
#define ALT_STDOUT_IS_JTAG_UART
#define ALT_STDOUT_PRESENT
#define ALT_STDOUT_TYPE "altera_avalon_jtag_uart"
#define ALT_SYSTEM_NAME "new_core"


/*
 * brake_light configuration
 *
 */

#define ALT_MODULE_CLASS_brake_light altera_avalon_pio
#define BRAKE_LIGHT_BASE 0x41020
#define BRAKE_LIGHT_BIT_CLEARING_EDGE_REGISTER 0
#define BRAKE_LIGHT_BIT_MODIFYING_OUTPUT_REGISTER 0
#define BRAKE_LIGHT_CAPTURE 0
#define BRAKE_LIGHT_DATA_WIDTH 4
#define BRAKE_LIGHT_DO_TEST_BENCH_WIRING 0
#define BRAKE_LIGHT_DRIVEN_SIM_VALUE 0
#define BRAKE_LIGHT_EDGE_TYPE "NONE"
#define BRAKE_LIGHT_FREQ 50000000
#define BRAKE_LIGHT_HAS_IN 0
#define BRAKE_LIGHT_HAS_OUT 1
#define BRAKE_LIGHT_HAS_TRI 0
#define BRAKE_LIGHT_IRQ -1
#define BRAKE_LIGHT_IRQ_INTERRUPT_CONTROLLER_ID -1
#define BRAKE_LIGHT_IRQ_TYPE "NONE"
#define BRAKE_LIGHT_NAME "/dev/brake_light"
#define BRAKE_LIGHT_RESET_VALUE 0
#define BRAKE_LIGHT_SPAN 16
#define BRAKE_LIGHT_TYPE "altera_avalon_pio"


/*
 * brake_pedal configuration
 *
 */

#define ALT_MODULE_CLASS_brake_pedal altera_avalon_pio
#define BRAKE_PEDAL_BASE 0x41060
#define BRAKE_PEDAL_BIT_CLEARING_EDGE_REGISTER 0
#define BRAKE_PEDAL_BIT_MODIFYING_OUTPUT_REGISTER 0
#define BRAKE_PEDAL_CAPTURE 0
#define BRAKE_PEDAL_DATA_WIDTH 1
#define BRAKE_PEDAL_DO_TEST_BENCH_WIRING 0
#define BRAKE_PEDAL_DRIVEN_SIM_VALUE 0
#define BRAKE_PEDAL_EDGE_TYPE "NONE"
#define BRAKE_PEDAL_FREQ 50000000
#define BRAKE_PEDAL_HAS_IN 1
#define BRAKE_PEDAL_HAS_OUT 0
#define BRAKE_PEDAL_HAS_TRI 0
#define BRAKE_PEDAL_IRQ -1
#define BRAKE_PEDAL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define BRAKE_PEDAL_IRQ_TYPE "NONE"
#define BRAKE_PEDAL_NAME "/dev/brake_pedal"
#define BRAKE_PEDAL_RESET_VALUE 0
#define BRAKE_PEDAL_SPAN 16
#define BRAKE_PEDAL_TYPE "altera_avalon_pio"


/*
 * hal configuration
 *
 */

#define ALT_INCLUDE_INSTRUCTION_RELATED_EXCEPTION_API
#define ALT_MAX_FD 32
#define ALT_SYS_CLK SYS_TIMER
#define ALT_TIMESTAMP_CLK SYS_TIMER


/*
 * jtag_uart configuration
 *
 */

#define ALT_MODULE_CLASS_jtag_uart altera_avalon_jtag_uart
#define JTAG_UART_BASE 0x41078
#define JTAG_UART_IRQ 1
#define JTAG_UART_IRQ_INTERRUPT_CONTROLLER_ID 0
#define JTAG_UART_NAME "/dev/jtag_uart"
#define JTAG_UART_READ_DEPTH 64
#define JTAG_UART_READ_THRESHOLD 8
#define JTAG_UART_SPAN 8
#define JTAG_UART_TYPE "altera_avalon_jtag_uart"
#define JTAG_UART_WRITE_DEPTH 64
#define JTAG_UART_WRITE_THRESHOLD 8


/*
 * left_turn configuration
 *
 */

#define ALT_MODULE_CLASS_left_turn altera_avalon_pio
#define LEFT_TURN_BASE 0x41050
#define LEFT_TURN_BIT_CLEARING_EDGE_REGISTER 0
#define LEFT_TURN_BIT_MODIFYING_OUTPUT_REGISTER 0
#define LEFT_TURN_CAPTURE 0
#define LEFT_TURN_DATA_WIDTH 3
#define LEFT_TURN_DO_TEST_BENCH_WIRING 0
#define LEFT_TURN_DRIVEN_SIM_VALUE 0
#define LEFT_TURN_EDGE_TYPE "NONE"
#define LEFT_TURN_FREQ 50000000
#define LEFT_TURN_HAS_IN 0
#define LEFT_TURN_HAS_OUT 1
#define LEFT_TURN_HAS_TRI 0
#define LEFT_TURN_IRQ -1
#define LEFT_TURN_IRQ_INTERRUPT_CONTROLLER_ID -1
#define LEFT_TURN_IRQ_TYPE "NONE"
#define LEFT_TURN_NAME "/dev/left_turn"
#define LEFT_TURN_RESET_VALUE 0
#define LEFT_TURN_SPAN 16
#define LEFT_TURN_TYPE "altera_avalon_pio"


/*
 * prog_mem configuration
 *
 */

#define ALT_MODULE_CLASS_prog_mem altera_avalon_onchip_memory2
#define PROG_MEM_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define PROG_MEM_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define PROG_MEM_BASE 0x20000
#define PROG_MEM_CONTENTS_INFO ""
#define PROG_MEM_DUAL_PORT 0
#define PROG_MEM_GUI_RAM_BLOCK_TYPE "AUTO"
#define PROG_MEM_INIT_CONTENTS_FILE "new_core_prog_mem"
#define PROG_MEM_INIT_MEM_CONTENT 1
#define PROG_MEM_INSTANCE_ID "NONE"
#define PROG_MEM_IRQ -1
#define PROG_MEM_IRQ_INTERRUPT_CONTROLLER_ID -1
#define PROG_MEM_NAME "/dev/prog_mem"
#define PROG_MEM_NON_DEFAULT_INIT_FILE_ENABLED 0
#define PROG_MEM_RAM_BLOCK_TYPE "AUTO"
#define PROG_MEM_READ_DURING_WRITE_MODE "DONT_CARE"
#define PROG_MEM_SINGLE_CLOCK_OP 0
#define PROG_MEM_SIZE_MULTIPLE 1
#define PROG_MEM_SIZE_VALUE 80000
#define PROG_MEM_SPAN 80000
#define PROG_MEM_TYPE "altera_avalon_onchip_memory2"
#define PROG_MEM_WRITABLE 1


/*
 * right_turn configuration
 *
 */

#define ALT_MODULE_CLASS_right_turn altera_avalon_pio
#define RIGHT_TURN_BASE 0x41040
#define RIGHT_TURN_BIT_CLEARING_EDGE_REGISTER 0
#define RIGHT_TURN_BIT_MODIFYING_OUTPUT_REGISTER 0
#define RIGHT_TURN_CAPTURE 0
#define RIGHT_TURN_DATA_WIDTH 3
#define RIGHT_TURN_DO_TEST_BENCH_WIRING 0
#define RIGHT_TURN_DRIVEN_SIM_VALUE 0
#define RIGHT_TURN_EDGE_TYPE "NONE"
#define RIGHT_TURN_FREQ 50000000
#define RIGHT_TURN_HAS_IN 0
#define RIGHT_TURN_HAS_OUT 1
#define RIGHT_TURN_HAS_TRI 0
#define RIGHT_TURN_IRQ -1
#define RIGHT_TURN_IRQ_INTERRUPT_CONTROLLER_ID -1
#define RIGHT_TURN_IRQ_TYPE "NONE"
#define RIGHT_TURN_NAME "/dev/right_turn"
#define RIGHT_TURN_RESET_VALUE 0
#define RIGHT_TURN_SPAN 16
#define RIGHT_TURN_TYPE "altera_avalon_pio"


/*
 * sys_timer configuration
 *
 */

#define ALT_MODULE_CLASS_sys_timer altera_avalon_timer
#define SYS_TIMER_ALWAYS_RUN 0
#define SYS_TIMER_BASE 0x41000
#define SYS_TIMER_COUNTER_SIZE 32
#define SYS_TIMER_FIXED_PERIOD 0
#define SYS_TIMER_FREQ 50000000
#define SYS_TIMER_IRQ 0
#define SYS_TIMER_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SYS_TIMER_LOAD_VALUE 49999
#define SYS_TIMER_MULT 0.001
#define SYS_TIMER_NAME "/dev/sys_timer"
#define SYS_TIMER_PERIOD 1
#define SYS_TIMER_PERIOD_UNITS "ms"
#define SYS_TIMER_RESET_OUTPUT 0
#define SYS_TIMER_SNAPSHOT 1
#define SYS_TIMER_SPAN 32
#define SYS_TIMER_TICKS_PER_SEC 1000
#define SYS_TIMER_TIMEOUT_PULSE_OUTPUT 0
#define SYS_TIMER_TYPE "altera_avalon_timer"


/*
 * sysid configuration
 *
 */

#define ALT_MODULE_CLASS_sysid altera_avalon_sysid_qsys
#define SYSID_BASE 0x41080
#define SYSID_ID 4660
#define SYSID_IRQ -1
#define SYSID_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SYSID_NAME "/dev/sysid"
#define SYSID_SPAN 8
#define SYSID_TIMESTAMP 1615169842
#define SYSID_TYPE "altera_avalon_sysid_qsys"


/*
 * turn_signal configuration
 *
 */

#define ALT_MODULE_CLASS_turn_signal altera_avalon_pio
#define TURN_SIGNAL_BASE 0x41030
#define TURN_SIGNAL_BIT_CLEARING_EDGE_REGISTER 0
#define TURN_SIGNAL_BIT_MODIFYING_OUTPUT_REGISTER 0
#define TURN_SIGNAL_CAPTURE 0
#define TURN_SIGNAL_DATA_WIDTH 2
#define TURN_SIGNAL_DO_TEST_BENCH_WIRING 0
#define TURN_SIGNAL_DRIVEN_SIM_VALUE 0
#define TURN_SIGNAL_EDGE_TYPE "NONE"
#define TURN_SIGNAL_FREQ 50000000
#define TURN_SIGNAL_HAS_IN 1
#define TURN_SIGNAL_HAS_OUT 0
#define TURN_SIGNAL_HAS_TRI 0
#define TURN_SIGNAL_IRQ -1
#define TURN_SIGNAL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define TURN_SIGNAL_IRQ_TYPE "NONE"
#define TURN_SIGNAL_NAME "/dev/turn_signal"
#define TURN_SIGNAL_RESET_VALUE 0
#define TURN_SIGNAL_SPAN 16
#define TURN_SIGNAL_TYPE "altera_avalon_pio"

#endif /* __SYSTEM_H_ */
