/*
 * main.c
 *
 *  Created on: May 1, 2019
 *      Author: Kim
 */


#include <stdio.h>
#include <unistd.h>
#include "system.h"
#include "altera_avalon_pio_regs.h"
#include <io.h>

int main(){
	int delay;
	int turn_signal;
	int brake_signal;

	while(1){
		turn_signal = IORD_ALTERA_AVALON_PIO_DATA(TURN_SIGNAL_BASE);
		brake_signal = IORD_ALTERA_AVALON_PIO_DATA(BRAKE_PEDAL_BASE);

		if(brake_signal == 1){
			IOWR_ALTERA_AVALON_PIO_DATA(BRAKE_LIGHT_BASE, 15);
		}else{
			IOWR_ALTERA_AVALON_PIO_DATA(BRAKE_LIGHT_BASE, 0);
		}
		if(turn_signal == 1){
			IOWR_ALTERA_AVALON_PIO_DATA(LEFT_TURN_BASE, 1);
			delay = 0;
			while(delay < 500000){
				delay++;
			}
			IOWR_ALTERA_AVALON_PIO_DATA(LEFT_TURN_BASE, 2);
			delay = 0;
			while(delay < 500000){
				delay++;
			}
			IOWR_ALTERA_AVALON_PIO_DATA(LEFT_TURN_BASE, 4);
			delay = 0;
			while(delay < 500000){
				delay++;
			}
		}else if(turn_signal == 2){
			IOWR_ALTERA_AVALON_PIO_DATA(RIGHT_TURN_BASE, 4);
			delay = 0;
			while(delay < 500000){
				delay++;
			}
			IOWR_ALTERA_AVALON_PIO_DATA(RIGHT_TURN_BASE, 2);
			delay = 0;
			while(delay < 500000){
				delay++;
			}
			IOWR_ALTERA_AVALON_PIO_DATA(RIGHT_TURN_BASE, 1);
			delay = 0;
			while(delay < 500000){
				delay++;
			}
		}
		else{
			IOWR_ALTERA_AVALON_PIO_DATA(LEFT_TURN_BASE,0);
			IOWR_ALTERA_AVALON_PIO_DATA(RIGHT_TURN_BASE,0);
		}
	}
}

