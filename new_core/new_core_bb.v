
module new_core (
	brake_light_external_connection_export,
	brake_pedal_external_connection_export,
	clk_clk,
	key_external_connection_export,
	left_turn_external_connection_export,
	reset_reset_n,
	right_turn_external_connection_export);	

	output	[3:0]	brake_light_external_connection_export;
	input		brake_pedal_external_connection_export;
	input		clk_clk;
	input	[1:0]	key_external_connection_export;
	output	[2:0]	left_turn_external_connection_export;
	input		reset_reset_n;
	output	[2:0]	right_turn_external_connection_export;
endmodule
