	component new_core is
		port (
			brake_light_external_connection_export : out std_logic_vector(3 downto 0);                    -- export
			brake_pedal_external_connection_export : in  std_logic                    := 'X';             -- export
			clk_clk                                : in  std_logic                    := 'X';             -- clk
			key_external_connection_export         : in  std_logic_vector(1 downto 0) := (others => 'X'); -- export
			left_turn_external_connection_export   : out std_logic_vector(2 downto 0);                    -- export
			reset_reset_n                          : in  std_logic                    := 'X';             -- reset_n
			right_turn_external_connection_export  : out std_logic_vector(2 downto 0)                     -- export
		);
	end component new_core;

	u0 : component new_core
		port map (
			brake_light_external_connection_export => CONNECTED_TO_brake_light_external_connection_export, -- brake_light_external_connection.export
			brake_pedal_external_connection_export => CONNECTED_TO_brake_pedal_external_connection_export, -- brake_pedal_external_connection.export
			clk_clk                                => CONNECTED_TO_clk_clk,                                --                             clk.clk
			key_external_connection_export         => CONNECTED_TO_key_external_connection_export,         --         key_external_connection.export
			left_turn_external_connection_export   => CONNECTED_TO_left_turn_external_connection_export,   --   left_turn_external_connection.export
			reset_reset_n                          => CONNECTED_TO_reset_reset_n,                          --                           reset.reset_n
			right_turn_external_connection_export  => CONNECTED_TO_right_turn_external_connection_export   --  right_turn_external_connection.export
		);

