	new_core u0 (
		.brake_light_external_connection_export (<connected-to-brake_light_external_connection_export>), // brake_light_external_connection.export
		.brake_pedal_external_connection_export (<connected-to-brake_pedal_external_connection_export>), // brake_pedal_external_connection.export
		.clk_clk                                (<connected-to-clk_clk>),                                //                             clk.clk
		.key_external_connection_export         (<connected-to-key_external_connection_export>),         //         key_external_connection.export
		.left_turn_external_connection_export   (<connected-to-left_turn_external_connection_export>),   //   left_turn_external_connection.export
		.reset_reset_n                          (<connected-to-reset_reset_n>),                          //                           reset.reset_n
		.right_turn_external_connection_export  (<connected-to-right_turn_external_connection_export>)   //  right_turn_external_connection.export
	);

